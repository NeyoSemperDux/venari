const { Command } = require('discord.js-commando');

module.exports = class NicknameCommand extends Command {
    constructor(venari) {
        super(venari, {
            name: 'nickname',
            group: 'sandbox',
            memberName: 'nickname',
            description: 'Prefixed Command to change the nickname of a user via AutoRole',
            guildOnly: true
        })
    }

    run(msg) {
        
    }
}