const { Command } = require('discord.js-commando');

module.exports = class DirectMessageCommand extends Command {
    constructor(venari) {
        super(venari, {
            name: 'dm',
            group: 'etc',
            memberName: 'dm',
            description: 'Sends a message to the user you mention.',
            examples: ['dm @User Hi there!'],
            args: [
                {
                    key: 'user',
                    prompt: 'Which user do you want to sent the DM to?',
                    type: 'user'
                },
                {
                    key: 'content',
                    prompt: 'What would you like the content of the message to be?',
                    type: 'string'
                }
            ]
        })
    }

    run(msg, { user, content }) {
        msg.delete();
        return user.send(content);
    }
}