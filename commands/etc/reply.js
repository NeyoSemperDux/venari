const { Command } = require('discord.js-commando');

module.exports = class ReplyCommand extends Command {
    constructor(venari) {
        super(venari, {
            name: 'reply',
            group: 'etc',
            memberName: 'reply',
            description: 'Replies with a Message.',
            examples: ['reply']
        });
    }

    async run(msg) {
        const message = await msg.say('Hi, I\'m awake!');

        return message.edit('I want to go to bed.');
    }
};