const { Command } = require('discord.js-commando');
const { RichEmbed } = require('discord.js');

module.exports = class StartCommand extends Command {
    constructor(venari) {
        super(venari, {
            name: 'start',
            group: 'forced-weekly-fun',
            memberName: 'start',
            description: 'Used to start Forced Weekly Fun',
            examples: ['start']
        })
    }

    async run(msg) {

        const embed = await new RichEmbed()
            .addField("**Let's see if this works.**")
            .setTimestamp();
        msg.delete();
        return msg.embed(embed);
    }
}