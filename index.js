// const { CommandoClient, SQLiteProvider } = require('discord.js-commando');
const { CommandoClient } = require('discord.js-commando');
const path = require('path');
// const sqlite = require('sqlite');

const config = require("./config.json");

const venari = new CommandoClient(
    {
        commandPrefix: config.prefix,
        owner: [config.ownerID, '181597006387609600'],
        unknownCommandResponse: false
    }
);

/* sqlite.open(path.join(__dirname, "settings.sqlite3")).then((db) => {
    helios.setProvider(new SQLiteProvider(db));
});*/

venari.registry
    .registerDefaultTypes()
    .registerGroups(
        [
            ['dolph', 'Commands requested by Dolph'],
            ['etc', 'Etc.'],
            ['forced-weekly-fun', 'Commands used to enforce Force Weekly Fun'],
            ['nostalgia', 'Nostalgic commands'],
            ['sandbox', 'Commands that are WIP by Neyo']
        ]
    )
    .registerDefaultGroups()
    .registerDefaultCommands()
    .registerCommandsIn(path.join(__dirname, 'commands'));

venari.on('ready', () => {
    console.log(`${config.name} is ready!`);
    venari.user.setActivity('Warframe');
});

venari.login(config.token);

// https://discordapp.com/api/oauth2/authorize?client_id=404943557762351115&permissions=8&scope=bot

// https://discordapp.com/api/oauth2/authorize?client_id=413156073105784842&permissions=8&scope=bot